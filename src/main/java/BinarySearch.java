/*
* A binary search implementation for integer arrays.
* */
public class BinarySearch {

    public static int binarySearch(int[] nums, int n) {
        int right = nums.length - 1;
        int left = 0;
        while (right >= left) {
            int mid = left + ((right - left) / 2);
            if (nums[mid] > n) {
                right = mid;
            } else if (nums[mid] < n) {
                left = mid;
            } else {
                return mid;
            }
        }
        return -1;
    }

}
