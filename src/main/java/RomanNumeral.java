/*
* A function for converting positive integers to their
* representation as Roman numerals
* */
public class RomanNumeral {

    enum Numeral {
        I(1), V(5), X(10), L(50), C(100), D(500), M(1000);
        int value;

        Numeral(int value) {
            this.value = value;
        }
    }

    /*
    * Convert positive integers to roman numerals
    * */
    public static String fromInt (int n) {

        if(n <= 0) {
            throw new IllegalArgumentException();
        }

        StringBuilder buf = new StringBuilder();

        final Numeral[] values = Numeral.values();
        for (int i = values.length - 1; i >= 0; i--) {
            while (n >= values[i].value) {
                buf.append(values[i]);
                n -= values[i].value;
            }
        }
        return buf.toString();
    }

}
